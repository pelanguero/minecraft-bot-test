const mineflayer = require("mineflayer")
const { pathfinder, Movements, goals: { GoalNear, GoalPlaceBlock, GoalFollow, GoalBlock, GoalBreakBlock } } = require('mineflayer-pathfinder')
const autoeat = require('mineflayer-auto-eat').plugin
const Vec3 = require('vec3');
const bot = mineflayer.createBot({
    host: 'localhost', // minecraft server ip
    username: 'bottest', // minecraft username
    auth: 'offline', // for offline mode servers, you can set this to 'offline'
    port: 57749               // only set if you need a port that isn't 25565
    // version: false,             // only set if you need a specific version or snapshot (ie: "1.8.9" or "1.16.5"), otherwise it's set automatically
    // password: '12345678'        // set if you want to use password-based auth (may be unreliable)
})
bot.loadPlugin(pathfinder)


bot.once('spawn', () => {
    //mineflayerViewer(bot, { port: 3000, version: "1.25.0", firstPerson: true }) // port is the minecraft server port, if first person is false, you get a bird's-eye view
    const currentPosition = bot.entity.position;
    //mcData = require('minecraft-data')(bot.version);
    console.log(`Posición actual del bot: x=${currentPosition.x}, y=${currentPosition.y}, z=${currentPosition.z}`);
    //followPlayer();
})

bot.on('chat', (username, message) => {
    if (username === bot.username) return
    //mover a
    if (message.startsWith("come")) {
        let splitedMessge = message.split(" ")
        let vec = new Vec3(parseInt(splitedMessge[1]), parseInt(splitedMessge[2]) + 1, parseInt(splitedMessge[3]))
        let goal = new GoalBlock(vec.x, vec.y, vec.z)


        moveto(vec, goal)
    }
    if (message.startsWith("transferd")) {
        //bot to chest
        let splitedMessge = message.split(" ")
        let vec = new Vec3(parseInt(splitedMessge[1]), parseInt(splitedMessge[2]), parseInt(splitedMessge[3]))
        depositItemstoChest(vec)
        
    }

    if (message.startsWith("transferu")) {
        //chest to bot
        let splitedMessge = message.split(" ")
        let vec = new Vec3(parseInt(splitedMessge[1]), parseInt(splitedMessge[2]), parseInt(splitedMessge[3]))
        takeItemsFromChest(vec)
    }

    if (message.startsWith("equip")) {
        //equipar armadura
        equipar();
    }
    if (message.startsWith("uequip")) {
        //equipar armadura
        desequipar();
    }
    
    if (message.startsWith("inventario")){
        console.log(bot.inventory.slots);
    }

    if (message.startsWith("herramienta")){
        hachaequipar();
    }





})

async function hachaequipar(){
    let itemc = bot.inventory.findInventoryItem('diamond_pickaxe')
    await bot.equip(itemc,"hand");
}

async function desequipar(){
     let itemc = bot.inventory.findInventoryItem('diamond_boots')
     await bot.unequip("feet");
}

async function equipar(){
     let itemc = bot.inventory.findInventoryItem('diamond_boots')
     await bot.equip(itemc,"feet");
}

async function moveto(vec, goal) {
    const mcData = require('minecraft-data')(bot.version)
    const movements = new Movements(bot, mcData)
    //movements.scafoldingBlocks = []

    bot.pathfinder.setMovements(movements)


    bot.chat("En camino")
    await bot.pathfinder.goto(goal)
    bot.chat("llegue")
}
//no
async function depositItemstoChest(vec) {
    const chestBlock = await bot.world.getBlock(vec)
    if (chestBlock) {
        let chest = await bot.openChest(chestBlock);
        
        for (slot of bot.inventory.slots) {
            //console.log(slot)
            if (slot) {
                await chest.deposit(slot.type, null, slot.count);
            }

        }
        chest.close();
    } else {
        console.log('No se encontró ningún cofre cercano');
    }
}

async function takeItemsFromChest(vec) {
    const chestBlock = await bot.world.getBlock(vec)
    if (chestBlock) {
        let chest = await bot.openChest(chestBlock);
        //console.log(chest);
        for (slot of chest.slots) {
            //console.log(slot)
            if (slot) {
                try{
                    await chest.withdraw(slot.type, null, slot.count);
                }catch(error){
                    console.log(error)
                }
                
            }

        }
        chest.close();
    } else {
        console.log('No se encontró ningún cofre cercano');
    }
}


function followPlayer() {
    const playerCI = bot.players['pelanguero']

    if (!playerCI || !playerCI.entity) {
        bot.chat("I can't see pelanguero!")
        return
    }

    const mcData = require('minecraft-data')(bot.version)
    const movements = new Movements(bot, mcData)
    //movements.scafoldingBlocks = []

    bot.pathfinder.setMovements(movements)

    const goal = new GoalFollow(playerCI.entity, 1)
    bot.pathfinder.setGoal(goal, true)
}

function lookAtNearestPlayer() {
    const playerFilter = (entity) => entity.type === 'player'
    const playerEntity = bot.nearestEntity(playerFilter)

    if (!playerEntity) return

    const pos = playerEntity.position.offset(0, playerEntity.height, 0)
    bot.lookAt(pos)
}

//bot.on('physicTick', lookAtNearestPlayer)